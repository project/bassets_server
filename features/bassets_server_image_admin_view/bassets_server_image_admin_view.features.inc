<?php
/**
 * @file
 * bassets_server_image_admin_view.features.inc
 */

/**
 * Implements hook_views_api().
 */
function bassets_server_image_admin_view_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
